exports.requiresLogin = function(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect("/login");
};

/* Examples: how to authorize private stuff */

/*

exports.user = {
    hasAuthorization : function (req, res, next) {
      if (req.profile.id != req.user.id) {
        return res.redirect('/users/'+req.profile.id)
      }
      next()
    }
}

exports.article = {
    hasAuthorization : function (req, res, next) {
      if (req.article.user.id != req.user.id) {
        return res.redirect('/articles/'+req.article.id)
      }
      next()
    }
}

*/
