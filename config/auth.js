var  LocalStrategy = require("passport-local").Strategy
    , User = require("../app/models").User
    ;

module.exports = function(passport, config) {
  passport.use(new LocalStrategy({
      usernameField: "email",
      passwordField: "password"
    },
    function(email, password, done) {
      User.find({where: {email: email}}).done(function(err, user) {
        if (err) { return done(err); }
        if (!user) {
          return done(null, false, {message: "Nombre de usuario incorrecto."});
        }
        if (!user.validPassword(password)) {
          return done(null, false, {message: "Contraseña incorrecta."});
        }
        return done(null, user);
      });
    }
  ));

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.find(id).done(done);
  });

};
