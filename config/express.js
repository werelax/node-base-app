var express = require("express")
  , flash = require("connect-flash");

module.exports = function(app, config, passport) {

  app.configure(function(){
    app.set('port', process.env.PORT || 3000);
    app.set('views', config.rootPath + '/app/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('your secret here'));
    // app.use(express.session({cookie: {maxAge: 600000}}));
    app.use(express.cookieSession());
    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(app.router);
    app.use(express.static(config.rootPath +  '/public'));
  });

  app.configure('development', function(){
    app.use(express.errorHandler());
  });
};
