module.exports = function(app, passport, auth) {

  /* user routes */

  var users = require("../app/controllers/users");
  app.get("/login", users.login);
  app.post("/session", passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login"
  }));
  app.get("/register", users.register);
  // app.get('/users', auth.requiresLogin, user.list);
  app.post("/users", users.create);

  /* app.param("userId", users.user); */

  /* post routes */
  var posts = require("../app/controllers/posts");
  app.get('/', auth.requiresLogin, posts.index);
  app.get('/posts/new', auth.requiresLogin, posts.new);
  app.post('/posts', auth.requiresLogin, posts.create);
  app.get('/posts/:id', auth.requiresLogin, posts.view);

  /* comments routes */
  var comments = require("../app/controllers/comments");
  app.post('/posts/:id/comments', auth.requiresLogin, comments.create);

  /* app.param("postId", posts.post); */
};
