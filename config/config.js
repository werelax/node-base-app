var path = require("path")
    , rootPath = path.normalize(__dirname + "/..")
    , templatePath = path.normalize(__dirname + "/../app/templates");


module.exports = {
  development: {
    db: {
      name: "reddix",
      username: "username",
      password: "passwd",
      storage: "./db/development.sqlite"
    },
    rootPath: rootPath,
    app: {
      name: "Redix? Memex? Reddix?"
    }
  },
  test: {
    db: {
      username: "username",
      password: "passwd",
      storage: ":memory:"
    },
    rootPath: rootPath,
    app: {
      name: "Redix? Memex? Reddix?"
    }
  },
  production: {}
};
