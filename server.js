/* Dependencies */

var express = require("express")
  , http = require("http")
  , passport = require("passport")
  , Sequelize = require("sequelize")
  ;

/* Configuration */

var env = process.env.NODE_ENV || "development"
  , config = require("./config/config")[env]
  , auth = require("./config/middlewares/authorization")
  ;

/* Bootstrap database */

var sequelize = new Sequelize(config.db.name, config.db.username, config.db.password, {
  dialect: "sqlite",
  storage: config.db.storage
});

/* Bootstrap models */

require("./app/models").initialize(sequelize, config);
// sequelize.sync();


/* Passport config */

require("./config/auth")(passport, config);

/* Express settings */

var app = express();
require("./config/express")(app, config, passport);

/* Routes */

require("./config/routes")(app, passport, auth);

/* Server */

var port = process.env.PORT || 3000;
app.listen(port);
console.log("Express app started on port", port);

exports = module.exports = app;

/*
http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

*/
