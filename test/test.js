process.env.NODE_ENV = "test";

var assert = require("assert")
  , Q = require("q")
  , models = require("../models")
  , db = models.sequelize
  , User = models.User
  , Post = models.Post
  , Comment = models.Comment
  ;

before(function(done) {
  db.sync({force: true}).done(done);
});

beforeEach(function(done) {
  ["Users", "Posts"].reduce(function(promise, table) {
    return promise.then(function() {
      var query = db.query("DELETE FROM " + table);
      return Q.nfcall(query.done.bind(query));
    })
  }, Q())
  .then(done);
});

/* User & Auth */

describe("User", function() {
  it("can determine if a password is valid", function(done) {
    User.create({email: "a@b.c", password: "1234"}).done(function(err, u) {
      assert.equal(err, null);
      assert.equal(u.validPassword("1234"), true);
      assert.equal(u.validPassword("1235"), false);
      done();
    });
  });
});

/* Post */

describe("Post", function() {
  var user;

  beforeEach(function(done) {
    User.create({email: "a@b.c", password: "1234"}).done(function(err, u) {
      user = u;
      done();
    });
  });

  it("can be created", function(done) {
    Post.create({title: "1", user: user}).done(function(err, post) {
      assert.equal(err, null);
      assert.equal(post.user, user);
      done();
    });
  });

});

describe("Post", function() {
  var user, post;

  beforeEach(function(done) {
    User.create({email: "a@b.c", password: "1234"}).done(function(err, u) {
      user = u;
      Post.create({title: "1"}).done(function(err, p) {
        post = p;
        done();
      });
    });
  });

  it("can be created", function(done) {
    Comment.create({text:"Hola!", post: post}).done(function(err, comment) {
      assert.equal(err, null);
      assert.equal(comment.text, "Hola!");
      assert.equal(comment.post, post);
      done();
    });
  });

});

/* Dummy user */
/*
var user1 = models.User.create({name: "Pepito", id: 1});

describe("Post (collection)", function() {
  beforeEach(function() {
    Post.deleteAll();
  });

  describe("Item creation", function() {

    it("should create a new post in the collection", function() {
      var newPost = Post.create({
        type: "url",
        user: user1,
        url: "http://www.google.com",
        title: "A great search engine"
      });

      assert.equal(Post.list().length, 1, "one item in the collection");
      assert.equal(newPost.user, user1, "the right user posted it");
    });

  });

  describe("Sorting", function() {
    it("should bubble the most voted", function() {
      var p1 = new models.Post({id: 1}),
          p2 = new models.Post({id: 2});
      p1.save(); p2.save();
      p1.upVote();
      var list = Post.list({sort: "votes"});
      assert.equal(list[0], p1);
      assert.equal(list[1], p2);
      p2.upVote(); p2.upVote();
      list = Post.list({sort: "votes"});
      assert.equal(list[0], p2);
      assert.equal(list[1], p1);
    });
  });
});

describe("Post (model)", function() {

  describe("Creation", function() {
    it("should have the right timestamp", function() {
      var p = new models.Post(),
          refTime = Date.now();
      assert.equal(refTime - p.created_at, 0);
    });

    it("should start with an empty comment array", function() {
      var p = new models.Post();
      assert.equal(p.comments.constructor, Array);
    })
  });

  describe("Modification", function() {
    it("should know how to save itself", function() {
      var p = new models.Post({id: 1, title: "Hey!"});
      p.title = "Modified";
      p.save();
      assert.equal(Post.find(1).title, "Modified");
    });
  });

  describe("Voting", function() {
    it("can be voted", function() {
      var p = new models.Post({id: 1, title: "1"});
      assert.equal(p.votes, 0);
      p.upVote();
      assert.equal(p.votes, 1);
      p.downVote();
      assert.equal(p.votes, 0);
    });
  });
});

describe("Comment (model)", function() {
});

describe("Comment (collection)", function() {
  it("should insert the first comment in the post tree", function(done) {
  });

  it("should insert a comment in the first level (parent: post)", function(done) {
  });

});

*/
