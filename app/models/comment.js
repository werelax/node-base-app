module.exports = function(sequelize, DataTypes) {
  return sequelize.define("Comment", {
    text: DataTypes.TEXT,
    votes: { type: DataTypes.INTEGER, defaultValue: 0 }
  }, {

    classMethods: {
      init: function(models) {
        with (models) {
          Comment
          .belongsTo(Post)
          .belongsTo(User);
        }
      }
    },

    instanceMethods: {
      upVote: function() {
        this.votes++;
      },
      downVote: function() {
        this.votes--;
      }
    }
  });
};
