var Sequelize = require("sequelize-sqlite").sequelize
  , R = require("../../lib/prelude")
  , mode = process.env.NODE_ENV || "dev"
  , fs = require("fs")
  ;

/* Bootstrap */

exports.initialize = function(sequelize, config) {

  var modelsPath = config.rootPath + '/app/models',
      models = fs.readdirSync(modelsPath).reduce(function(models, file) {
        if (!file.match(".swp") && !file.match("index.js")) {
          var name = file[0].toUpperCase() + file.slice(1, -3);
          models[name] = sequelize.import(modelsPath + "/" + file);
        }
        return models;
      }, {});

  /* Initialize the models */

  for (var name in models) if (models[name].init) {
    models[name].init(models);
  }

  /* Exports */

  R.augment(module.exports, models);
};
