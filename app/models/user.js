/* User model */
var crypto = require("crypto");

module.exports = function(sequelize, DataTypes) {
  return sequelize.define("User", {
    id: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    name: DataTypes.STRING
  }, {

    classMethods: {
      init: function(models) {
        with (models) {
          User
          .hasMany(Post)
          .hasMany(Comment);
        }
      }
    },

    instanceMethods: {
      validPassword: function(password) {
        return password === this.password;
      },
      gravatarHash: function() {
        var email = this.email.toLowerCase().trim(),
            md5hash = crypto.createHash("md5").update(email);
        return md5hash.digest("hex");
      },
      gravatar: function() {
        return "http://www.gravatar.com/avatar/" + this.gravatarHash();
      }
    }
  });
};
