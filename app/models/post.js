module.exports = function(sequelize, DataTypes) {
  return sequelize.define("Post", {
    title: DataTypes.TEXT,
    type: DataTypes.INTEGER,
    url: DataTypes.STRING,
    text: DataTypes.TEXT,
    votes: { type: DataTypes.INTEGER, defaultValue: 0 }
  }, {

    classMethods: {
      init: function(models) {
        with (models) {
          Post
          .belongsTo(User)
          .hasMany(Comment);
        }
      }
    },

    instanceMethods: {
      upVote: function() {
        this.votes++;
      },
      downVote: function() {
        this.votes--;
      }
    }
  });
};
