var models = require("../models")
    , Post = models.Post;

exports.index = function(req, res) {
  Post.findAll({order: "votes DESC", include: [models.User]})
  .success(function(posts) {
    console.log(posts[0].user.gravatar());
    res.render('posts/index.jade', {posts: posts});
  });
};

exports.new = function(req, res) {
  res.render("posts/new.jade");
};

exports.create = function(req, res) {
  Post.create({
    title: req.body.title,
    url: req.body.url,
    text: req.body.text,
    UserId: req.user.id
  }).done(function() {
    res.redirect("/");
  });
};

exports.view = function(req, res) {
  /* TODO: Refactor this! */
  Post.find(req.params.id).success(function(post) {
    post.getComments()
    .success(function(comments) {
      post.getUser()
      .success(function(user) {
        post.user = user;
        res.render("posts/view.jade", {
          post: post,
          comments: comments,
          error: req.flash("error")
        });
      });
    })
  });
};
