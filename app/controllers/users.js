var models = require("../models")
    , User = models.User;

exports.login = function(req, res) {
  res.render("users/login.jade");
};

exports.register = function(req, res) {
  res.render("users/register.jade");
};

exports.create = function(req, res) {
   var email = req.body.email,
        password = req.body.password,
        passwordConfirm = req.body.passwordConfirm,
        name = req.body.name;

    if (password === passwordConfirm) {
      User.create({
        email: email,
        password: password,
        name: name
      })
      .success(function(user) {
        req.login(user, function(err) {
          if (err) { res.redirect("/register"); }
          return res.redirect("/");
        });
      })
      .error(function(e) {
        req.flash("error", "Can't create user");
        res.redirect("/register");;
      });
    } else {
      req.flash("error", "Passwords don't match");
      res.redirect("/register");;
    }
};
