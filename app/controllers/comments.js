var models = require("../models")
    , Comment = models.Comment;

exports.create = function(req, res) {
  /* TODO: refactor this! */
  var Post = models.Post,
      postId = req.params.id,
      text = req.body.comment;

  Post.find(postId)
  .success(function(post) {
    Comment.create({text: text, PostId: post.id, UserId: req.user.id})
    .success(function(comment) {
      /* maybe a flash or something */
    })
    .error(function() {
      req.flash("error", "Can't create comment");
      /* maybe a flash or something */
    })
    .done(function() {
      res.redirect("/posts/" + postId);
    });
  })
  .error(function() {
    req.flash("error", "Can't find post");
    res.redirect("/posts/" + postId);
  });
};
